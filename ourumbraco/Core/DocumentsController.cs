﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Umbraco.Cms.Web.Common.Controllers;

namespace ourumbraco.Core
{
    public class DocumentsController : UmbracoApiController
    {
        public IEnumerable<string> GetAllDocuments()
        {
            return new[] { "Secret file", "JFK document", "World Domination plans", "Shopping list from Mum" };
        }
    }
}
