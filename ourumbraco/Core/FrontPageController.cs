﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.Controllers;

namespace ourumbraco.Core
{
	public class FrontPageController : RenderController
	{
		public FrontPageController(ILogger<FrontPageController> logger,
			ICompositeViewEngine compositeViewEngine,
			IUmbracoContextAccessor umbracoContextAccessor)
			: base(logger, compositeViewEngine, umbracoContextAccessor)
		{
		}
		public override IActionResult Index()
		{
			return Content("HelloWorld!!"); //return CurrentTemplate(CurrentPage);
		}
	}
}