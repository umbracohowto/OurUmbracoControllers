# OurUmbraco

This repo contains a super basic implementation of 4 types of Controllers, it also includes a postman collection that can be used to test the endpoints, for response times etc..
![alt text](https://gitlab.com/umbracohowto/ourumbracoControllers/-/raw/main/PostMan.png)


# SurfaceController #
https://localhost:44320/umbraco/surface/mysurface/index/

The Surface controller is a really powerful controller where you can do anything.
If you wanted to do a script where you change one member type to another, then you can use the surface controller. 

The below example is where we output a string "Hallo world from my surface controller" to the front end when you visit the url on the above. 
Though that could be done with a RenderController instead.

You actually do not need a view to make the surface controller work. It just works without it.
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Umbraco.Cms.Core.Cache;
using Umbraco.Cms.Core.Logging;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Infrastructure.Persistence;
using Umbraco.Cms.Web.Website.Controllers;

namespace ourumbraco.Core
{
    public class MySurfaceController : SurfaceController
    {
        public MySurfaceController(
            IUmbracoContextAccessor umbracoContextAccessor,
            IUmbracoDatabaseFactory databaseFactory,
            ServiceContext services,
            AppCaches appCaches,
            IProfilingLogger profilingLogger,
            IPublishedUrlProvider publishedUrlProvider)
            : base(umbracoContextAccessor, databaseFactory, services, appCaches, profilingLogger, publishedUrlProvider) 
        { 
        }

        public IActionResult Index()
        {
            return Content("Hallo world from my surface controller");
        }
    }
}
```

# RenderController #
https://localhost:44327/

The Render Controller is mainly used for when you want to hijack a view in Umbraco. 
Say you wanted to add something ontop of the view.

While not nearly as powerful as a Surface controller, it can still help with getting a lot of complicated stuff added to a view.

```
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.Controllers;

namespace ourumbraco.Core
{
	public class FrontPageController : RenderController
	{
		public FrontPageController(ILogger<FrontPageController> logger,
			ICompositeViewEngine compositeViewEngine,
			IUmbracoContextAccessor umbracoContextAccessor)
			: base(logger, compositeViewEngine, umbracoContextAccessor)
		{
		}
		public override IActionResult Index()
		{
			return Content("HelloWorld!!"); // CurrentTemplate(CurrentPage);
		}
	}
}
```

# MvcController #
https://localhost:44320/mymvc/

Has not been updated for v9
```
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace UmbracoV8.Core
{
    public class MyMvcController : Controller
    {
        // GET: MyMvc
        public ActionResult Index()
        {
            return Content("HelloWorld!!");
        }
    }

    public class RegisterMyMvcControllerComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<MyMvcController>(Lifetime.Request);
            composition.Components().Append<MyComponent>();
        }
    }
	public class MyComponent : IComponent
	{
		public void Initialize()
		{
			RouteTable.Routes.MapRoute(
				name: "mymvc",
				url: "mymvc",
				new { controller = "MyMvc", action = "Index" }
			);
		}

        public void Terminate()
        {
        }
    }
}
```

# UmbracoApiController #
https://localhost:44327/umbraco/api/documents/getalldocuments 

You use this controller to integrate third party services with your Umbraco project.
You can get stuff from the database with this controller, that is implanted in custom tables and do all kind of wonderous thing with it. 

Forexample if you had a ecommerce plugin installed and you wanted to display the clients last bought orders. You would then output the orders at an endpoint, which the frontend developers could then use to make some nice UI for the client.

```
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Umbraco.Cms.Web.Common.Controllers;

namespace ourumbraco.Core
{
    public class DocumentsController : UmbracoApiController
    {
        public IEnumerable<string> GetAllDocuments()
        {
            return new[] { "Secret file", "JFK document", "World Domination plans", "Shopping list from Mum" };
        }
    }
}
```
__The below still need to be updated__
About Controllers in Umbraco you may read the following documentation
https://our.umbraco.com/documentation/Implementation/Controllers/


```
{
	"Umbraco backoffice user" : {
		"username": "test@test.com",
		"password": "Mgj]O}7o{p"
	}
}
```

About optimization...

"Optimization hinders evolution."-- Alan Perlis"

Premature optimization is the root of all evil."-- Donald Knuth"

Rules of optimization:
●Rule 1: Don't do it.
●Rule 2 (for experts only): Don't do it yet."-- Michael A. Jackson (Kidding)
This doesn’t mean you shouldn’t test for performance, or anything like that. It means that you shouldn’t complicate your code up front, in an attempt to get good performance. Simplicity first.

KISS : https://en.wikipedia.org/wiki/KISS_principle
